var canvas = document.getElementById('game');
var context = canvas.getContext('2d');


var laser = [];
var expl=[];
var asteroid = [];
var timer = 0;


var fonimg = new Image();
fonimg.src = '/resources/img/fon.jpg';
var asteroidimg = new Image();
asteroidimg.src = '/resources/img/aster.png';
var xwingimg = new Image();
xwingimg.src = '/resources/img/xwing.png';
var xwing = {x: 600, y: 650};
var laserimg = new Image();
laserimg.src = '/resources/img/laser.png';
var explimg = new Image();
explimg.src ='/resources/img/expl.png';
var gameOverImg = new Image();
gameOverImg.src = '/resources/img/GameOver1.png';
var gameOver = {x: 600, y: 650};
var score = 0;
var enemyImg = new Image();
enemyImg.src ='/resources/img/enemy1.png';
var enemy = {x: 100, y: 150};
var enemyLaserImg = new Image();
enemyLaserImg.src = '/resources/img/laserEnemy.png';



    fonimg.onload = function () {
    game();
}
  
 

//start settings

canvas.addEventListener("mousemove", function (event) {
    xwing.x = event.offsetX - 25;
    xwing.y = event.offsetY - 13;
});


//lead game loop
 function game() {
    update();
    render();
    requestAnimationFrame(game);

}



function update() {
//move asteroid
    var timer100 = timer % 100;
	var timer80 = timer % 80;
	var timer50 = timer % 50;
	var timer20 = timer % 20;
	timer100++;
    if (timer100 % 100 == 0) {
        asteroid.push({
            x: Math.random() * 1200,
            y: -10,
            dx: Math.random() * 2 - 1,
            dy: Math.random() * 2 + 1,
            del: 0
        });
    }
    //shoot laser
	timer++
    if (timer % 50 == 0) {
        laser.push({x: xwing.x + 5, y: xwing.y, dx: 0, dy: -5.2});
       // laser.push({x: xwing.x + 10, y: xwing.y, dx: 0.5, dy: -5});
          laser.push({x: xwing.x + 30, y: xwing.y, dx: 0, dy: -5});
    }

    //анимация +10
      for (i in expl){
      expl[i].animx=expl[i].animx+0.1;
       if(expl[i].animx>5){expl[i].animy++; expl[i].animx=0}
      if(expl[i].animy>5) expl.splice(i,1)
   }

    //физика
    for (i in laser) {
        laser[i].x = laser[i].x + laser[i].dx;
        laser[i].y = laser[i].y + laser[i].dy;
        if (laser[i].y < -10) laser.splice(i, 1);
    }

    for (i in asteroid) {
        asteroid[i].x = asteroid[i].x + asteroid[i].dx;
        asteroid[i].y = asteroid[i].y + asteroid[i].dy;




            //границы
            if (asteroid[i].x >= 1300 || asteroid[i].x < 0 || asteroid[i].y >= 850) {
                asteroid.splice(i, 1);
                break;
            }
            // if (asteroid[i].y >= 850) asteroid.splice(i, 1);


    //проверка астеройда на столкновение с пулей
    for (j in laser) {
        if (Math.abs(asteroid[i].x + 15 - laser[j].x - 5) < 45 && Math.abs(asteroid[i].y - laser[j].y) < 10) {
            //столкновение
            
			score += 10;
		    document.getElementById('count').innerHTML = score;
			console.log(score);
            //взрыв
            expl.push({x:asteroid[i].x-2,y:asteroid[i].y-2, animx:0,animy:0});

            //помечаем астероид на удаление
            asteroid[i].del = 1;
            laser.splice(j, 1);
            break;
           
           
        }
    }
    //проверка астеройда на столкновение с кораблем(конец игры)
    for (j in xwing) {
            if (Math.abs(asteroid[i].x + 15 - xwing.x - 5) < 45 && Math.abs(asteroid[i].y - xwing.y) < 10) {
                //столкновение
            var resultScore = score;
           // document.getElementById('resultScore').innerHTML = resultScore;
            console.log(resultScore);
                //взрыв
                expl.push({x:asteroid[i].x-2,y:asteroid[i].y-2, animx:0,animy:0});

                //помечаем астероид на удаление
                asteroid[i].del = 1;
                xwing = 0;
	

                break;

            }
        }
//удаляем астеройды
    if (asteroid[i].del == 1) asteroid.splice(i, 1);
	
    }




}




function render() {
       //отрисовка фона
       if(xwing != 0){ context.drawImage(fonimg, 0, 0, 1400, 900);}else {
		   context.drawImage(gameOverImg,0,0,1400,900);
	   }


   if(xwing !=0){ //отрисовка коробля
        context.drawImage(xwingimg, xwing.x, xwing.y, 60, 80);
   }else{
	   xwing =0;
   }


if(xwing!=0){//отрисовка астеройдов
        for (i in asteroid) context.drawImage(asteroidimg, asteroid[i].x, asteroid[i].y, 80, 80);
}else{
	asteroid.splice(i, 1);
}

 if(xwing !=0){   //отрисовка лазера
        for (i in laser) context.drawImage(laserimg, laser[i].x, laser[i].y, 20, 18);
 }else{
	 laser.splice(j, 1);
 }

	//отрисовка +10 за астеройд
    for(i in expl) context.drawImage(explimg,328*Math.floor(expl[i].animx),
       328*Math.floor(expl[i].animy),328,328,expl[i].x,expl[i].y,30,30);

}

