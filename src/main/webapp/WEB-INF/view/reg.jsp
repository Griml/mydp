
<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" language="java" %>
<html>
<head>
    <title>REGISTRATION FORM</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/style.css">
</head>
<body class = 'body'>
<h1 align="center" style="color:#00FF00">REGISTRATION FORM</h1>
<form method='post' class='reg-form'>

    <div class='form-row'>
        <label style="color:#00FF00" for='firstName'>First Name: </label>
        <input type='text' id='firstName' name='firstName'>
    </div>

    <div class='form-row'>
        <label style="color:#00FF00" for='lastName'>Last Name: </label>
        <input type='text' id='lastName' name='lastName'>
    </div>

    <div class='form-row'>
        <label style="color:#00FF00" for='form_email'>Email: </label>
        <input type='email' id='form_email' name='email'>
    </div>

    <div class='form-row'>
        <label style="color:#00FF00" for='form_login'>Login: </label>
        <input type='text' id='form_login' name='login'>
    </div>

    <div class='form-row'>
        <label style="color:#00FF00" for='form_password'>Password: </label>
        <input type="password" id='form_password' name='password'></input>
    </div>

    <div class="form-row">
        <input style="color: blue" type="submit" value='SUBMIT'>
    </div>
    <tr>
        <td style="color:#DC143C">${msg}</td>
    </tr>
</form>

</body>
</html>
