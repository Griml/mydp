<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Asteroids game</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/style.css">
</head>
<body>
<div id="wrapper">
    <canvas id='game' width="1400" height="900"> Элемент не поддерживается</canvas>
</div>

<div class="menu">
    <table class="button">

        <tr class="session">
            <th><h5 style="color:#00ff00">Hello: ${userInSession.login}</h5></th>
        </tr>
        <tr>
            <th><input id="startGame" type="button" value="START GAME" onClick='location.href="/start"'></th>
        </tr>
        <tr>
            <th><input id="leaderBoard" type="button" value="LEADER BOARD" onClick='location.href="/start"'></th>
        </tr>
        <tr>
            <th><input id="exitGame" type="button" value="EXIT GAME" onClick='location.href="https://google.com"'></th>
        </tr>
        <tr class="score">
            <th>SCORE:</th>
            <td id='count'></td>
        </tr>
    </table>


    <script src="${pageContext.request.contextPath}/resources/game.js"></script>
</div>
</body>
</html>