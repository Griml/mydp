
<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Asteroids game</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/style.css">
</head>
<body class = 'body'>
<div id="buttonpanel">
    <table class="button">
        <tr>
            <th><input id = "startGame" type="button" value="START GAME"
                       onClick='location.href="${pageContext.request.contextPath}/start"'></th>
        </tr>
        <tr>
            <th><input id = "leaderBoard" type="button" value="LEADER BOARD"
                       onClick='location.href="${pageContext.request.contextPath}/start"'></th>
        </tr>
        <tr>
            <th><input id = "exitGame" type="button" value="EXIT GAME"
                       onClick='location.href="https://google.com"'></th>
        </tr>
    </table>
</div>
<form method="post" action="${pageContext.request.contextPath}/login" class="loginform">
    <table>
        <tr>
            <th><%--@declare id="uname"--%>
                <label for="uname"><b>
                <h1 style="color:#9ACD32">Username</h1></b></label></th>
            <td><input type="text" placeholder="Enter Username" name="login" required></td>
        </tr>
        <tr>
            <th>
                <%--@declare id="psw"--%>
                    <label for="psw"><b><h1 style="color:#9ACD32">Password</h1></b></label>
            </th>
            <td>
                <input type="password" placeholder="Enter Password" name="password" required>
            </td>
        </tr>
        <tr>
            <th>
                <button type="submit">Login</button>
            </th>
        </tr>
        <tr>
            <th>
                <input id="registration" type="button" value= "Registration" onClick='location.href="${pageContext.request.contextPath}/reg"'>
            </th>
        </tr>
        <tr>
            <label>
                <td style="color:#9ACD32">
                    <input type="checkbox" checked="checked" name="remember">Remember me
                </td>
            </label>
        </tr>
        <tr>
            <td style="color:red">${msg}</td>
        </tr>
    </table>
</form>
</body>
</html>
