package dao;

import java.util.List;

public interface GenericReg<T> {
    void save(T data);
    void update(T data);
    T find (String login);
    List<T> findAll();

}
