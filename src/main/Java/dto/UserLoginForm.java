package dto;

public class UserLoginForm {
    private String login;
    private String password;
    private boolean remember;

    @Override
    public String toString() {
        return "UserLoginForm{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", remember=" + remember +
                '}';
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isRemember() {
        return remember;
    }

    public void setRemember(boolean remember) {
        this.remember = remember;
    }
}

