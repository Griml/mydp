package controller;

import domain.entity.User;
import dto.UserRegistrationForm;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class RegController {

    @PostMapping("/reg")
    public String register (UserRegistrationForm userRegistrationForm, RedirectAttributes redirectAttributes, Model model){
        System.out.println(userRegistrationForm);
        String msg = "ONE OR MORE FIELD(s) ARE EMPTY";
        if     (userRegistrationForm.getFirstName().isEmpty() ||
                userRegistrationForm.getLastName().isEmpty() ||
                userRegistrationForm.getEmail().isEmpty() ||
                userRegistrationForm.getLogin().isEmpty() ||
                userRegistrationForm.getPassword().isEmpty()) {
            redirectAttributes.addFlashAttribute("msg", msg);
            System.out.println("ONE OR MORE FIELD(s) ARE EMPTY");
           return "redirect:/reg";
        } else {
            return "redirect:/";
        }

    }

}
