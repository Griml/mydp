package controller;

import domain.entity.User;
import dto.UserLoginForm;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;

@Controller
@SessionAttributes("userInSession")
public class LoginController {

    @PostMapping("/login")
    public String login(UserLoginForm userForm, RedirectAttributes attr, Model model) {
        System.out.println(userForm);

        if(userForm.getLogin().equals("user") && userForm.getPassword().equals("user")) {
            model.addAttribute("userInSession", new User(userForm.getLogin(),
                                                             userForm.getPassword()));
            return "redirect:/start";
        } else {
            attr.addFlashAttribute("msg", "Login or Password are incorrect");
            return "redirect:/";
        }
    }
}
