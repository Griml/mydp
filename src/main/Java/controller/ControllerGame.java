package controller;

import domain.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;

import java.util.ArrayList;
import java.util.List;

@Controller
@SessionAttributes("usersInSession")
public class ControllerGame {

    // @ResponseBody                                             //Анотация для ответа в виде строки
    @GetMapping(value = "/")    //Метод действия обрабатывает запрос по пути startPage
    public String index() {
        return "index";
    }

    @GetMapping(value = "/start")
    public String gameStart(@SessionAttribute User userInSession) {
        System.out.println("In session: " + userInSession);
        return "start";
    }

    @GetMapping(value = "/reg")
    public String registration(){
        return "reg";
    }
}

