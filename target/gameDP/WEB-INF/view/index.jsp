
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Asteroids game</title>
    <link rel="stylesheet" href="../source/style.css">

</head>
<body>
<div id="wrapper">
    <canvas id='game' width="600" height="600"> Элемент не поддерживается</canvas>
</div>
<script type="text/javascript" src="../source/game.js"></script>
</body>
</html>
