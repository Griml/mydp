var canvas = document.getElementById('game');
var context = canvas.getContext('2d');


var laser = [];
var expl=[];
var asteroid = [];
var timer = 0;


var fonimg = new Image();
fonimg.src = 'source/img/fon.jpg';
var asteroidimg = new Image();
asteroidimg.src = 'source/img/aster.png';
var xwingimg = new Image();
xwingimg.src = 'source/img/xwing.png';
var xwing = {x: 600, y: 650};
var laserimg = new Image();
laserimg.src = 'source/img/laser.png';
var explimg =new Image();
explimg.src ='source/img/expl.png';





fonimg.onload = function () {
    game();
}

//start settings

canvas.addEventListener("mousemove", function (event) {
    xwing.x = event.offsetX - 25;
    xwing.y = event.offsetY - 13;
});


//lead game loop
function game() {
    update();
    render();
    requestAnimationFrame(game);
}



function update() {
//move asteroid
    timer++;
    if (timer % 20 == 0) {
        asteroid.push({
            x: Math.random() * 1200,
            y: -10,
            dx: Math.random() * 2 - 1,
            dy: Math.random() * 2 + 1,
            del: 0
        });
    }
    //shoot laser
    if (timer % 20 == 0) {
        laser.push({x: xwing.x + 5, y: xwing.y, dx: 0, dy: -5.2});
        //laser.push({x: xwing.x + 10, y: xwing.y, dx: 0.5, dy: -5});
          laser.push({x: xwing.x + 30, y: xwing.y, dx: 0, dy: -5});
    }



    //анимация +10
      for (i in expl){
      expl[i].animx=expl[i].animx+0.1;
       if(expl[i].animx>5){expl[i].animy++; expl[i].animx=0}
      if(expl[i].animy>5) expl.splice(i,1)
   }


//физика
    for (i in laser) {
        laser[i].x = laser[i].x + laser[i].dx;
        laser[i].y = laser[i].y + laser[i].dy;
        if (laser[i].y < -10) laser.splice(i, 1);
    }

    for (i in asteroid) {
        asteroid[i].x = asteroid[i].x + asteroid[i].dx;
        asteroid[i].y = asteroid[i].y + asteroid[i].dy;




            //границы
            if (asteroid[i].x >= 1190 || asteroid[i].x < 0 || asteroid[i].y >= 850) {
                asteroid.splice(i, 1);
                break;
            }
            // if (asteroid[i].y >= 850) asteroid.splice(i, 1);


    //проверка астеройда на столкновение с пулей
    for (j in laser) {
        if (Math.abs(asteroid[i].x + 15 - laser[j].x - 5) < 50 && Math.abs(asteroid[i].y - laser[j].y) < 15) {
            //столкновение

            //взрыв
            expl.push({x:asteroid[i].x-2,y:asteroid[i].y-2, animx:0,animy:0});

            //помечаем астероид на удаление
            asteroid[i].del = 1;
            laser.splice(j, 1);
            break;
        }
    }
//удаляем астеройды
    if (asteroid[i].del == 1) asteroid.splice(i, 1);
    }
}

function render() {
       //отрисовка фона
        context.drawImage(fonimg, 0, 0, 1400, 900);
    //отрисовка коробля
        context.drawImage(xwingimg, xwing.x, xwing.y, 60, 80);
    //отрисовка астеройдов
        for (i in asteroid) context.drawImage(asteroidimg, asteroid[i].x, asteroid[i].y, 80, 80);

    //отрисовка лазера
        for (i in laser) context.drawImage(laserimg, laser[i].x, laser[i].y, 20, 18);
    //отрисовка +10 за астеройд
    for(i in expl) context.drawImage(explimg,328*Math.floor(expl[i].animx),
       328*Math.floor(expl[i].animy),328,328,expl[i].x,expl[i].y,30,30);



}

/*var requestAnimFrame = function() {
    return window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        function (callback) {
            window.setTimeout(callback, 1000 / 20);

        };
}*/